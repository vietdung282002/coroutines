package com.example.coroutines

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.coroutines.adapter.NewsAdapter
import com.example.coroutines.mvvm.MainViewModel

class MainActivity : AppCompatActivity() {
    private val apiKey = "pub_346686fde36e1541e312917e79cafb55b9658"
    private val lang = "en"
    private lateinit var viewModel: MainViewModel
    private val newsListAdapter = NewsAdapter(this,arrayListOf())
    private lateinit var recyclerView: RecyclerView
    private lateinit var textView: TextView
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView = findViewById(R.id.listError)
        progressBar = findViewById(R.id.loadingView)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]

        viewModel.refresh(apiKey,lang)

        recyclerView = findViewById(R.id.rvNews)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = newsListAdapter
        observeViewModel()
    }

    private fun observeViewModel() {
        viewModel.news.observe(this){
            it?.let {
                recyclerView.visibility = View.VISIBLE
                newsListAdapter.update(it)
            }
        }
        viewModel.newsLoadError.observe(this){
            textView.visibility = if(it == "") View.GONE else View.VISIBLE
        }
        viewModel.loading.observe(this){
            it?.let {
                progressBar.visibility = if(it) View.VISIBLE else View.GONE
                if(it){
                    textView.visibility = View.GONE
                    recyclerView.visibility = View.GONE
                }
            }
        }
    }
}