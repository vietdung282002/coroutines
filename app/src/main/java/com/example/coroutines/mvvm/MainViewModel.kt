package com.example.coroutines.mvvm

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.coroutines.models.News
import com.example.coroutines.network.API
import kotlinx.coroutines.*

class MainViewModel() : ViewModel() {
    private var job: Job? = null
    private val exceptionHandler = CoroutineExceptionHandler{ _, throwable ->
        onError("Exception handled: ${throwable.localizedMessage}")
    }

    val news = MutableLiveData<List<News>>()
    val newsLoadError = MutableLiveData<String?>()
    val loading = MutableLiveData<Boolean>()

    private fun onError(message: String) {
        newsLoadError.value = message
        loading.value = false
    }

    fun refresh(apiKey: String, lang: String) {
        fetchUsers(apiKey,lang)

    }

    private fun fetchUsers(apiKey: String, lang: String) {
        loading.value = true
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            val response = API.apiService.getNews(apiKey, lang)
            withContext(Dispatchers.Main){
                if(response.isSuccessful){
                    news.value = response.body()?.results
                    newsLoadError.value = null
                    loading.value = false
                }else {
                    onError("Error : ${response.message()} ")
                }
            }
        }
        newsLoadError.value = ""
        loading.value = false
    }

    override fun onCleared() {
        super.onCleared()
        job?.cancel()
    }
}