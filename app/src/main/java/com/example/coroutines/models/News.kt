package com.example.coroutines.models

data class NewsApiResponse(
    val results: List<News>
)

data class News(
    val title: String,
    val description: String,
    val pubDate: String,
    val image_url: String,
)
