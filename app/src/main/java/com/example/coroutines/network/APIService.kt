package com.example.coroutines.network

import com.example.coroutines.models.NewsApiResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {
    @GET("/api/1/news")
    suspend fun getNews(
        @Query("apikey") apiKey: String,
        @Query("language") language: String
    ): Response<NewsApiResponse>
}

