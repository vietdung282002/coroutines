package com.example.coroutines.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object API{
    private var retrofit: Retrofit? = null
    private const val baseUrl = "https://newsdata.io"

    val apiService: APIService
        get() {
            if(retrofit == null){
                retrofit = Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit!!.create(APIService::class.java)
        }
}
